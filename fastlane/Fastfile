# This file contains the fastlane.tools configuration
# You can find the documentation at https://docs.fastlane.tools
#
# For a list of all available actions, check out
#
#     https://docs.fastlane.tools/actions
#
# For a list of all available plugins, check out
#
#     https://docs.fastlane.tools/plugins/available-plugins
#

# Uncomment the line if you want fastlane to automatically update itself
# update_fastlane

require 'net/http'
require 'json'

GITLAB_TOKEN = ENV['gitlab_api_access_token']
PROJECT_ID = ENV['CI_PROJECT_ID']
REF = ENV['CI_COMMIT_REF_NAME']
JOB = ENV['CI_JOB_NAME']
API_ROOT = ENV['CI_API_V4_URL']

def public_key
  uri = URI("#{API_ROOT}/projects/#{PROJECT_ID}/jobs/artifacts/#{REF}/raw/appetize-information.json?job=#{JOB}")
  http = Net::HTTP.new(uri.host, uri.port)
  http.use_ssl = true
  req = Net::HTTP::Get.new(uri)
  req['PRIVATE-TOKEN'] = GITLAB_TOKEN
  response = http.request(req)
  return '' if response.code.equal?('404')

  appetize_info = JSON.parse(response.body)
  appetize_info['publicKey']
end

def update_deployment_url(pub_key)
  File.open('../appetize-information.json', 'w') do |f|
    f.write(JSON.generate(publicKey: pub_key))
  end
  File.open('../deploy.env', 'w') do |f|
    f.write("APPETIZE_PUBLIC_KEY=#{pub_key}")
  end
end

default_platform(:android)

platform :android do

  desc "Builds the debug code"
  lane :buildDebug do
    gradle(task: "assembleDebug")
  end

  desc "Builds the release code"
  lane :buildRelease do
    gradle(task: "assembleRelease")
  end

  desc "Runs all the tests"
  lane :test do
    gradle(task: "test")
  end

  desc 'Pushes the app to Appetize and updates a review app'
  lane :review do
    appetize(api_token: ENV['APPETIZE_TOKEN'],
             public_key: public_key,
             path: 'app/build/outputs/apk/debug/app-debug.apk',
             platform: 'android')
    update_deployment_url(lane_context[SharedValues::APPETIZE_PUBLIC_KEY])
  end

  desc "Submit a new Internal Build to Play Store"
  lane :internal do
    upload_to_play_store(track: 'internal', apk: 'app/build/outputs/apk/release/app-release.apk')
  end

  # `skip_upload_changelogs` from https://github.com/fastlane/fastlane/issues/15681#issuecomment-556617987
  desc "Promote Internal to Alpha"
  lane :promote_internal_to_alpha do
    upload_to_play_store(track: 'internal', skip_upload_changelogs: true, track_promote_to: 'alpha')
  end

  # `skip_upload_changelogs` from https://github.com/fastlane/fastlane/issues/15681#issuecomment-556617987
  desc "Promote Alpha to Beta"
  lane :promote_alpha_to_beta do
    upload_to_play_store(track: 'alpha', skip_upload_changelogs: true, track_promote_to: 'beta')
  end

  # `skip_upload_changelogs` from https://github.com/fastlane/fastlane/issues/15681#issuecomment-556617987
  desc "Promote Beta to Production"
  lane :promote_beta_to_production do
    upload_to_play_store(track: 'beta', skip_upload_changelogs: true,  track_promote_to: 'production')
  end
end
