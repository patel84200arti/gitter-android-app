package im.gitter.gitter.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;

import im.gitter.gitter.R;
import im.gitter.gitter.models.CreateRoomModel;

public class PrivacyDialog extends DialogFragment {

    private Delegate delegate;
    private CreateRoomModel model;
    private int selected;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        delegate = (Delegate) activity;
        model = delegate.getModel(this);
        selected = model.isPrivate() ? 1 : 0;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        CharSequence[] options = new CharSequence[] {
                createOption(R.string.create_room_public, R.string.create_room_public_description),
                createOption(R.string.create_room_private, R.string.create_room_private_description)
        };

        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.create_room_privacy)
                .setSingleChoiceItems(options, selected, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        selected = which;
                    }
                })
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (selected > -1) {
                            model.setPrivate(selected == 1);
                        }
                    }
                })
                .create();
    }

    private CharSequence createOption(int titleResource, int descriptionResource) {
        CharSequence title = getString(titleResource);
        SpannableString description = new SpannableString(getString(descriptionResource));
        description.setSpan(new RelativeSizeSpan(0.75f), 0, description.length(), 0);
        description.setSpan(new ForegroundColorSpan(Color.GRAY), 0, description.length(), 0);
        return TextUtils.concat(title, "\n", description);
    }

    public interface Delegate {
        CreateRoomModel getModel(PrivacyDialog dialog);
    }
}
